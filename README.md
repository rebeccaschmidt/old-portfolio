# Rebeccas Portfolie ![Build Success](https://gitlab.com/rebeccaschmidt/portfolio/badges/master/pipeline.svg)

Portfolio is deployed to https://rebeccaschmidt.gitlab.io/portfolio/.

Edit the index.html to add/remove images. Remember to also put them into the `portfolio_images` folder.